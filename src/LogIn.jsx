import { Component } from "react";
import axios from "axios";
import './styles.css';

class LogIn extends Component {

  state = {
    userName: "",
    password: "",
    labelText: ""
  };

  handleUserNameInput = e => {
    this.setState({ userName: e.target.value });
    this.setState({ labelText: ""});
  };

  handlePasswordInput = e => {
    this.setState({ password: e.target.value });
    this.setState({ labelText: ""});
  };

  Submit = () => {
    axios.post('/Test', { 
      userName: this.state.userName,
      password: this.state.password
    })
    .then(response => response.data)
    .then(data => this.setState({ labelText: `${data}`}));
  };

  render() {
  return (
    <div className="LogIn">
      <header className="LogIn-header">
      <div class="form-container">
        <h1>Login</h1>
        <section>
          <div className="input-container">
            <input type="text" name="username" placeholder="Username" required onChange={this.handleUserNameInput}/>
          </div>
          <div className="input-container">
            <input type="password" name="password" placeholder="Password" required onChange={this.handlePasswordInput}/>
          </div>
          <div className="button-container">
            <button type="button" onClick={this.Submit}>Submit</button>
          </div>
          <div className="label-container">
            <label> { this.state.labelText } </label>
          </div>
        </section>
      </div>
      </header>
    </div>
  );
  }
}

export default LogIn;